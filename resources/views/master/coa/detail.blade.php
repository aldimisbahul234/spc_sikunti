@extends('layouts/root')
@section('main')
        <div class="container">
        <div class="content">
             <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Student</span>
        </nav>
    <div class="row">

<div class="col-md-6 col-xl-3">
    <a class="block text-center" >
        <div class="block-content block-content-full">
              @if($student->avatar ==null)
                        <img src="{{asset('assets/media/avatars/avatar0.jpg')}}" style="width: 90%">
                        @else
            <img src="{{$url}}/storage/avatar/{{$student->avatar}}" style="width: 90%">
            @endif
                </div>
                    <div class="block-content block-content-full bg-body-light">
                    <div class="font-w600 mb-5">Siswa</div>
                </div>
            </a>
        </div>

  <div class="col-md-12 col-xl-6">
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Data siswa</h3>
            </div>
                <div class="block-content">
                    <p>Nama <b>{{$student->name}}</b></p>
                    <p>Email <b>{{$student->email}}</b></p>
                    <p>Nis <b>{{$student->nis}}</b></p>
                    </div>
                </div>
                 <a href="{{ url('student/edit/'.$student->student_id)}}" class="btn btn-warning">Edit</a>
                <a href="{{url('student')}}" class="btn btn-secondary">Back</a>

            </div>
        </div>

    </div>
</div>

@endsection