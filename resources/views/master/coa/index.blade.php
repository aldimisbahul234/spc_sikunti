@extends('layouts/root')
@section('main')
<div class="container">
	 <div class="content">
         <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Coa</span>
        </nav>
       @if ($errors->any())
            <div class="alert alert-danger m-t-20">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            Coa Data Table
        </h2>

        {{-- search --}}

       <!--  <a href="{{ url('coa/create') }}" class="btn btn-success mb-3"> <i class="fa fa-plus"></i> &nbsp; Create student</a> -->
       <!--  <button class="btn btn-success btn-import mb-3" type="button"> <i class="fa fa-upload"></i> &nbsp; Import Question</button> -->

         <!-- Filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Searching</h3>
            </div>
            <div class="block-content">
                <form action="{{ url('coa') }}" class="form-inline" method="GET">
                    <div class="form-group" style="width: 25%">
                        <label>Import coa</label>
                       <input type="file" name="">
                    </div>
                    <div class="form-group" style="margin-left:10px; width: 51%">
                        <label for="">Search</label>
                        <input type="text" autocomplete="off" class="form-control" id="value" name="value" value="{{ Request::get('value') }}" style="width: 100%" placeholder="Search">
                    </div>
                    <div class="form-group" style="margin:20px 0 0 10px; width: 21.5%">
                        <button class="btn btn-primary" style="width: 100%"> <i class="fa fa-search"></i> Cari</button>
                    </div>
                </form> <br>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">List of Coa</h3>
            </div>
            <div class="block-content" style="padding: 18px;">
                <table id="datatable" class="table table-vcenter table-stripped table-bordered">
                    <thead>
                        <tr>
                            <th></th>                            
                            <th>Name</th>
                            <th>Email</th>
                             <th>Nis</th>
                            <th>Address</th>
                            <th width="15%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(sizeof($student) > 0)
                            @foreach($student as $key => $value)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>                                    
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->nis }}</td>
                                    <td>{{ $value->address }}</td>
                                    <td>
                                        <a href="{{ url('student/detail/'. $value->student_id) }}" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></a>
                                        <a href="{{ url('student/edit/'. $value->student_id) }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                                        <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('student/delete/'. $value->student_id) }}" class="btn btn-sm btn-danger btn-xs btn-delete" >
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="6">coa not available.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                 
            </div>
        </div>
        <!-- END Table -->
</div>
</div>
@include('modal_delete')
@endsection
@push('script')
<script type="text/javascript">
     $(document).ready(function() {
            $('#datatable').dataTable({
                
                   dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
        });
    });
     $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
</script>
@endpush