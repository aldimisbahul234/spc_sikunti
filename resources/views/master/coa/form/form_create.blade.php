<form class="form-participant" action="{{ url('student') }}" method="post" enctype="multipart/form-data">
        @csrf  
	 	 <div class="row">              
            <input type="hidden" name="level_id" value="4">
            <div class="col-md-3 col-sm-12">
                <div class="block">
                        
                    <div class="block-header" style="padding-bottom: 0px; padding-left: 40px;padding-top: 45px;" >
                        <img src="{{asset('assets/media/avatars/avatar0.jpg')}}" style="width: 90%">
                    </div>
                    <div class="block-content" style="padding-top: 20px;padding-bottom: 30px; padding-left: 45px;">
                        <div class="form-group row">
                            <div class="col-12">
                                <input type="file" name="avatar">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-12">
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">PERSONAL INFORMATION</h3>
                    </div>
                    <div class="block-content">                
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" placeholder="Name" value="" id="name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" placeholder="email" name="email" id="email">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Nis</label>
                                        <input type="text" id="nis" name="nis" class="form-control" placeholder="nis" id="nis">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Alamat</label>
                                        <input type="text" name="address" class="form-control" placeholder="address" value="" id="address"> 
                                    </div>
                                </div>
                            </div>
                        </div>          
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Save</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                            <a href="{{ url('/') }}" class="btn btn-secondary" style="float:right;">Back</a>
                        </div>
</form>