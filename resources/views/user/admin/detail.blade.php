@extends('layouts/root')
@section('main')
        <div class="container">
        <div class="content">
             <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Users</span>
        </nav>
    <div class="row">

<div class="col-md-6 col-xl-3">
    <a class="block text-center" >
        <div class="block-content block-content-full">
              @if($profile->avatar ==null)
                        <img src="{{asset('assets/media/avatars/avatar0.jpg')}}" style="width: 90%">
                        @else
            <img src="{{$url}}/storage/avatar/{{$profile->avatar}}" style="width: 90%">
            @endif
                </div>
                    <div class="block-content block-content-full bg-body-light">
                    <div class="font-w600 mb-5">{{ $profile->username }}</div>
                </div>
            </a>
        </div>

  <div class="col-md-12 col-xl-6">
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Data user</h3>
            </div>
                <div class="block-content">
                    <p>Nama <b>{{$profile->name}}</b></p>
                    <p>Email <b>{{$profile->email}}</b></p>
                    </div>
                </div>
                 <a href="{{ url('user/edit/'.$profile->id)}}" class="btn btn-warning">Edit</a>
                <a href="{{url('user')}}" class="btn btn-secondary">Back</a>

            </div>
            
        </div>

    </div>
</div>

@endsection