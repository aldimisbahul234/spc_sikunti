@extends('layouts/root')
@section('main')
<div class="bg-body-light border-b">
        <div class="content py-5 text-center">
            <nav class="breadcrumb bg-body-light mb-0">
                <a class="breadcrumb-item" href="/">User</a>
                 @if(str_contains(url()->current(), '/user/create'))
            <span class="breadcrumb-item active">Create</span>
            @else
            <span class="breadcrumb-item active">Edit</span>
            @endif
        </nav>
    </div>
</div>
<div class="container">
	 <div class="content">
        @if(str_contains(url()->current(), '/user/create'))
            @include('user/admin/form/form_create')
        @else
            @include('user/admin/form/form_edit')
        @endif
	 </div>
</div>
@endsection
@push('script')
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        $('.form-participant').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': {
                    required: true,
                },
                 'email': {
                    required: true,
                },
                
            },
            messages: {
                'name': {
                    required: 'name has required',
                },
                'email': {
                    required: 'email has required',
                },
               
            }
        });
    </script>
@endpush
