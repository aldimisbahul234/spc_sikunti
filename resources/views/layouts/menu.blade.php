  <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-glass page-header-inverse main-content-boxed">
  <nav id="sidebar">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Side Header -->
                    <div class="content-header content-header-fullrow px-15">
                        <!-- Mini Mode -->
                        <div class="content-header-section sidebar-mini-visible-b">
                            <!-- Logo -->
                            <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                                <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                            </span>
                            <!-- END Logo -->
                        </div>
                        <!-- END Mini Mode -->

                        <!-- Normal Mode -->
                        <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                            <!-- Close Sidebar, Visible only on mobile screens -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times text-danger"></i>
                            </button>
                            <!-- END Close Sidebar -->

                            <!-- Logo -->
                            <div class="content-header-item">
                                <a class="link-effect font-w700" href="/">
                                    <i class="si si-fire text-primary"></i>
                                    <span class="font-size-xl text-dual-primary-dark">SPC</span> <span class="font-size-xl text-primary">SIKUNTI</span>
                                </a>
                            </div>
                            <!-- END Logo -->
                        </div>
                        <!-- END Normal Mode -->
                    </div>
                    <!-- END Side Header -->

                    <!-- Side User -->
                    <div class="content-side content-side-full content-side-user px-10 align-parent">
                        <!-- Visible only in mini mode -->
                        <div class="sidebar-mini-visible-b align-v animated fadeIn">
                            <img class="img-avatar img-avatar32" src="assets/media/avatars/avatar15.jpg" alt="">
                        </div>
                        <!-- END Visible only in mini mode -->

                        <!-- Visible only in normal mode -->
                        <div class="sidebar-mini-hidden-b text-center">
                            <a class="img-link" href="be_pages_generic_profile.html">
                                <img class="img-avatar" src="assets/media/avatars/avatar15.jpg" alt="">
                            </a>
                            <ul class="list-inline mt-10">
                                <li class="list-inline-item">
                                    <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase" href="{{url('user/detail/'.Auth::user()->id)}}">{{ Auth::user()->name }}</a>
                                </li>
                                <li class="list-inline-item">
                                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                    <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                                        <i class="si si-drop"></i>
                                    </a>
                                </li>
                               <!--  <li class="list-inline-item">
                                    <a class="link-effect text-dual-primary-dark" href="op_auth_signin.html">
                                        <i class="si si-logout"></i>
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                        <!-- END Visible only in normal mode -->
                    </div>
                    <!-- END Side User -->

                    <!-- Side Navigation -->
                    <div class="content-side content-side-full">
                        <ul class="nav-main">
                            <li>
                                <a href="/"><i class="si si-cup"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                            </li>
                           


                            <li class="nav-main-heading"><span class="sidebar-mini-visible">ST</span><span class="sidebar-mini-hidden">System Section</span></li>
                            <li class="{{ Request::is('level') || Request::is('level/*') || Request::is('module') || Request::is('module/*') || Request::is('task') || Request::is('task/*') || Request::is('role') || Request::is('role/*') ? 'open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">System</span></a>
                                <ul>
                                    <li>
                                        <a href="{{ url('level') }}" class="{{ Request::is('level') || Request::is('level/*') ? 'active' : '' }}">Level User</a>
                                    </li>
                                   <!--  <li>
                                        <a href="{{ url('module') }}" class="{{ Request::is('module') || Request::is('module/*') ? 'active' : '' }}">Module</a>
                                    </li>
                                     <li>
                                        <a href="{{ url('task') }}" class="{{ Request::is('task') || Request::is('task/*') ? 'active' : '' }}">Task</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('role') }}" class="{{ Request::is('role') || Request::is('role/*') ? 'active' : '' }}">Role</a>
                                    </li> -->
                                   
                                </ul>
                            </li>
                           
                           
                           
                            <li class="nav-main-heading"><span class="sidebar-mini-visible">US</span><span class="sidebar-mini-hidden">User Section</span></li>
                            <li class=" {{ Request::is('user') || Request::is('user/*') ? 'open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-user"></i><span class="sidebar-mini-hide">Users</span></a>
                                <ul>
                                    <li>
                                        <a href="{{ url('user') }}" class="{{ Request::is('user') || Request::is('user/*') ? 'active' : '' }}">Admin</a>  
                                    </li>
                                     <li>
                                        <a href="{{ url('user') }}" class="{{ Request::is('user') || Request::is('user/*') ? 'active' : '' }}">Client</a>  
                                    </li>
                                </ul>
                            </li>
                           
                            <li class="nav-main-heading"><span class="sidebar-mini-visible">PG</span><span class="sidebar-mini-hidden">Master data</span></li>
                            <li class=" {{ Request::is('student') || Request::is('student/*') || Request::is('product') || Request::is('product/*') || Request::is('stock') || Request::is('stock/*') ? 'open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-globe-alt"></i><span class="sidebar-mini-hide">Import</span></a>
                                <ul>
                                    <li>
                                        <a href="{{url('coa')}}" class="{{ Request::is('coa') || Request::is('coa/*') ? 'active' : '' }}">Coa & Saldo Awal</a>
                                    </li>
                                    <li>
                                        <a href="{{url('stock')}}" class="{{ Request::is('stock') || Request::is('stock/*') ? 'active' : '' }}">Buku Kas / Bank / ADJ</a>
                                    </li>
                                    <li>
                                        <a href="{{url('student')}}" class="{{ Request::is('student') || Request::is('student/*') ? 'active' : '' }}">Buku Penjualan</a>
                                    </li>
                                    <li>
                                        <a href="{{url('stock')}}" class="{{ Request::is('stock') || Request::is('stock/*') ? 'active' : '' }}">Buku Pembelian</a>
                                    </li>
                                    <li>
                                        <a href="{{url('student')}}" class="{{ Request::is('student') || Request::is('student/*') ? 'active' : '' }}">Suplier / Customer</a>
                                    </li>
                                </ul>
                            </li>
                           
                            </li>
                        </ul>
                    </div>
                    <!-- END Side Navigation -->
                </div>
                <!-- Sidebar Content -->
            </nav>
            <!-- END Sidebar -->