<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <title>Base-App</title>
        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="assets/media/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="assets/media/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/media/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="{{ asset('assets/css/codebase.min.css')}}">
        <style type="text/css">
        </style>
    </head>
    <body>
<!-- Sidebar -->
@include('layouts/menu')
<!-- Header -->
@include('layouts/header')
<br><br>
<!-- Main Container -->
<main id="main-container">
<!-- Hero -->

<!-- END Hero -->
@yield('main')
  </main>
<!-- END Main Container -->
@include('layouts/footer')
<!-- END Footer -->
    </body>
      <script src="{{ asset('assets/js/codebase.core.min.js')}}"></script>
        <script src="{{ asset('assets/js/codebase.app.min.js')}}"></script>

         <!-- Page JS Plugins -->
        <script src="{{ asset('assets/js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{ asset('assets/js/plugins/easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
        <script src="{{ asset('assets/js/plugins/chartjs/Chart.bundle.min.js')}}"></script>
        <script src="{{ asset('assets/js/plugins/flot/jquery.flot.min.js')}}"></script>
        <script src="{{ asset('assets/js/plugins/flot/jquery.flot.pie.min.js')}}"></script>
        <script src="{{ asset('assets/js/plugins/flot/jquery.flot.stack.min.js')}}"></script>
        <script src="{{ asset('assets/js/plugins/flot/jquery.flot.resize.min.js')}}"></script>

        <!-- Page JS Code -->
        <script src="{{ asset('assets/js/pages/be_pages_ecom_dashboard.min.js')}}"></script>
        <script src="{{ asset('assets/js/pages/be_comp_charts.min.js')}}"></script>

        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.css') }}">
<script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
<script src="{{ asset('assets/js/pages/be_tables_datatables.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        @stack('script')        
</html>