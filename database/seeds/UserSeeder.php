<?php

use Illuminate\Database\Seeder;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('users')->insert([
            [
				'name' => 'devadmin',
				'email' => 'devadm@gmail.com',
                'username' => 'devadm',
                'level_id' => 1,
				'password' => Hash::make('admin890'),
				'created_at' => '2020-10-13 10:35:09',
				'updated_at' => '2020-10-13 10:35:09',
            ], 
             [
                'name' => 'devadmin1',
                'email' => 'devadm1@gmail.com',
                'username' => 'devadm1',
                'level_id' => 2,
                'password' => Hash::make('sedingink4mu'),
                'created_at' => '2020-10-13 10:35:09',
                'updated_at' => '2020-10-13 10:35:09',
            ],  
             [
                'name' => 'devadmin2',
                'email' => 'devadm2@gmail.com',
                'username' => 'devadm2',
                'level_id' => 2,
                'password' => Hash::make('sedingink4mu'),
                'created_at' => '2020-10-13 10:35:09',
                'updated_at' => '2020-10-13 10:35:09',
            ],  
             [
                'name' => 'devadmin3',
                'email' => 'devadm3@gmail.com',
                'username' => 'devadm3',
                'level_id' => 2,
                'password' => Hash::make('sedingink4mu'),
                'created_at' => '2020-10-13 10:35:09',
                'updated_at' => '2020-10-13 10:35:09',
            ],  
             [
                'name' => 'devadmin4',
                'email' => 'devadm4@gmail.com',
                'username' => 'devadm4',
                'level_id' => 2,
                'password' => Hash::make('sedingink4mu'),
                'created_at' => '2020-10-13 10:35:09',
                'updated_at' => '2020-10-13 10:35:09',
            ],         
        ]);
    }
}
