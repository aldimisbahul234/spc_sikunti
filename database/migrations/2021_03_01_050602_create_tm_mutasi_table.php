<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmMutasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_mutasi', function (Blueprint $table) {
            $table->bigIncrements('mut_id');
            $table->date('mut_tgl')->nullable();
            $table->integer('coa_id')->nullable();
            $table->string('mut_ket')->nullable();
            $table->integer('mut_debet')->nullable();
            $table->integer('mut_kredit')->nullable();
            $table->string('mut_sumber')->nullable();
            $table->integer('tahun')->nullable();
            $table->integer('status')->nullable();
            $table->string('mut_nobukti')->nullable();
            $table->integer('mut_npwp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_mutasi');
    }
}
