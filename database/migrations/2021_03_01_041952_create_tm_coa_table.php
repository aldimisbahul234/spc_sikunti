<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmCoaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_coa', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('coa_akun');
            $table->string('coa_nb');
            $table->bigInteger('coa_saldo_d');
            $table->bigInteger('coa_saldo_k');
            $table->bigInteger('tahun');
            $table->string('coa_klasifikasi');
            $table->string('coa_header');
            $table->string('coa_subheader');
            $table->string('coa_detail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_coa');
    }
}
