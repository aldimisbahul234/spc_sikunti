<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEqPkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eq_pk', function (Blueprint $table) {
            $table->bigIncrements('eq_pk_id');
             $table->string('eq_pk_masa')->nullable();
             $table->string('eq_pk_faktur')->nullable();
             $table->string('eq_pk_nominal')->nullable();
             $table->string('eq_pk_tahun')->nullable();
             $table->string('eq_pk_jenis')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eq_pk');
    }
}
