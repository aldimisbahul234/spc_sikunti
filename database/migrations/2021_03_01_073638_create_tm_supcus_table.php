<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmSupcusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_supcus', function (Blueprint $table) {
            $table->bigIncrements('sc_id');
             $table->string('sc_npwp')->nullable();
             $table->string('sc_nama')->nullable();
             $table->string('sc_debet')->nullable();
             $table->string('sc_kredit')->nullable();
             $table->string('sc_type')->nullable();
             $table->string('sc_tahun')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_supcus');
    }
}
