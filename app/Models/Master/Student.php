<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = "students";

    protected $primaryKey = "student_id";
    protected $fillable = [
      'name','email','nis','avatar','address'
    ];

}
