<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\System\Level;
class LevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 25;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $url = env('URL');
       $level  = Level::orderBy('level');

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $level->where($key,'like','%'. $value .'%');
        }

        $level    = $level->paginate($this->limit);
        $level->appends($request->all());
    return view('system.level.index',compact('level','url'));       
          
    }

    public function store(Request $request)
    {
    	$user 			    = new \App\User;
    	$user->name 		= $request->name;
        $user->username     = $request->username;
        $user->level_id     = 2;
    	$user->email 		= $request->email;
    	$user->password	    = bcrypt($request->password);
    
        $insert = $user->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

    	return redirect('user');

    }

    public function create()
    {
      
    }

    public function edit($id)
    {
    
    }

    public function update(Request $request, $id)
    {
    
    }

    public function delete(Request $request, $id)
    {
    	
    }

    public function profile()
    {

    }

    
}
