<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\Student;
class CoaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 65;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $url = env('URL');
       $student  = Student::orderBy('name');

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $student->where($key,'like','%'. $value .'%');
        }

        $student    = $student->paginate($this->limit);
        $student->appends($request->all());
    return view('master.coa.index',compact('student','url'));       
          
    }

    public function store(Request $request)
    {
    	$student 			      = new Student;
    	$student->name 		      = $request->name;
        $student->email           = $request->email;
    	$student->nis 		  = $request->nis;
    	$student->password	      = bcrypt($request->password);
         if ($request->HasFile('avatar')) {
                $destination_path = "/public/admin/avatar/";
                $image = $request->file('avatar');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('avatar')->storeAs($destination_path,$image_name);
                $student['avatar'] = $image_name;
        }
        $insert = $student->save();

        if ($insert) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Student has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

    	return redirect('user');

    }

    public function create()
    {
        $url = env('URL');
     
        return view('master.student.form',compact('url'));
    }

    public function edit($id)
    {
        $url = env('URL');
        $student = Student::find($id);
        return view('master.student.form',compact('student','url'));
    }

    public function update(Request $request, $id)
    {
    $data = [
            'name' => $request->name,
            'email' => $request->email,
            'nis' => $request->nis,
            'address' => $request->address,
        ];
        if ($request->HasFile('avatar')) {
                $destination_path = "/public/avatar/";
                $image = $request->file('avatar');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('avatar')->storeAs($destination_path,$image_name);
                $data['avatar'] = $image_name;
        }
       if ($request->password ? : []) {
                 $data['password'] = bcrypt($request->password);
            }

        $update = Student::where('student_id',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Admin');
        }

        return redirect('student');
    
    }

    public function delete(Request $request, $id)
    {
    	
    }

    public function detail($id)
    {
        $url = env('URL');
        $student = Student::find($id);
        return view('master.student.detail',compact('student','url'));
    }

    
}
