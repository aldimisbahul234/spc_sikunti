<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUser;
use Illuminate\Support\Facades\Cache;
use Redis;
use App\Models\Level\Level;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->module = "User";
        $this->limit = 25;
    }

    /**
     * Show the application level.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $url = env('URL');
    	// $this->authorize('index', [ \App\User::class, $this->module ]);        
        //  $user = Cache::rememberForever("user_all",function () {
        //     return \App\User::all();
        // });
        // $user= Cache::get('user_all');
          $user              = \App\User::whereNotIn('level_id', [4,5,6,7,8])->orderBy('id', 'desc');

        if (\Auth::user()->level_id == 2)
            $user->where('level_id','!=',1);

        if( isset($request->key) && isset($request->value) )
        {
            $key    = $request->key;
            $value  = $request->value;

            $user->where($key,'like','%'. $value .'%');
        }

        $user    = $user->paginate($this->limit);
        $user->appends($request->all());
       return view('user.admin.index', compact('user','url'));
          
    }

    public function store(Request $request)
    {
    	$user 			    = new \App\User;
    	$user->name 		= $request->name;
        $user->username     = $request->username;
        $user->level_id     = 2;
    	$user->email 		= $request->email;
    	$user->password	    = bcrypt($request->password);
        
        if ($request->HasFile('avatar')) {
                $destination_path = "/public/admin/avatar/";
                $image = $request->file('avatar');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('avatar')->storeAs($destination_path,$image_name);
                $user['avatar'] = $image_name;
        }
        $insert = $user->save();

        if ($insert) {
            $insert = Cache::delete('user_all');
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been added');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to add Admin');
        }

    	return redirect('user');

    }

    public function create()
    {
         $url = env('URL');
        $levels = \App\Models\Level\Level::whereNotIn('level_id',[1,2,4,5])->get(['level_id','level']);
        
    	return view('user.admin.form', compact('levels','url'));
    }

    public function edit($id)
    {
    	$user = \App\User::find($id);
         $url = env('URL');

    	return view('user.admin.form', compact('user','url'));
    }

    public function update(Request $request, $id)
    {
    //	$this->authorize('edit', [ \App\User::class, $this->module ]);

    	$data = [
            'name' => $request->name,
            'username' => $request->username,
            'level_id' => $request->level_id,
            'email' => $request->email
        ];
        if ($request->HasFile('avatar')) {
                $destination_path = "/public/avatar/";
                $image = $request->file('avatar');
                $image_name = $image->getClientOriginalName();
                $path = $request->file('avatar')->storeAs($destination_path,$image_name);
                $data['avatar'] = $image_name;
        }
    	if ($request->password ? : []) {
                 $data['password'] = bcrypt($request->password);
            }

    	$update = \App\User::where('id',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been edited');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to edit Admin');
        }

        if ($request->type == "profile") {
            return redirect('profile');
        }

        return redirect('user');
    }

    public function delete(Request $request, $id)
    {
    	

    	$delete	= \App\User::where('id',$id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Admin has been deleted');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Failed to delete Admin');
        }

    	return redirect('user');
    }

    public function profile($id)
    {
        $url = env('URL');
        $profile    = \App\User::find($id);

        return view('user.admin.detail', compact('profile','url'));
    }

    public function profileUpdate(Request $request)
    {
        $this->authorize('update', [ \App\User::class, $this->module ]);

        $id = \Auth::user()->id;

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'phone' => $request->phone,
            'address' => $request->address,
        ];

        if ($request->password != "" || $request->password != NULL) {
            $data['password'] = bcrypt($request->password);
        }

        // return $data; exit;

        $update = \App\User::where('id',$id)->update($data);

        if ($update) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'User berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'User gagal diubah');
        }

        return redirect('profile');
    }
    public function approved(Request $request,$id)
    {

        $model  = \App\User::findOrFail($id);
        $model = [
            'verified' => 1,
        ];
        if ($model) {

           $update = \App\User::where('id',$id)->update($model);
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'approved has been edited');
       }
       return redirect()->back();
    }
}
