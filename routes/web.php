<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('login'); });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['namespace' => 'User'], function(){
    // Routing Group Menu Users
    Route::group(['prefix' => 'user'], function(){
        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@store');
        Route::get('/create', 'UserController@create');
        Route::get('/edit/{id}', 'UserController@edit');
         Route::get('/detail/{id}', 'UserController@profile');
        Route::put('/update/{id}', 'UserController@update');
        Route::any('/delete/{id}', 'UserController@delete');
        Route::post('/approved/{id}', 'UserController@approved');
    });
});
Route::group(['namespace' => 'System'], function(){
    // Routing Group Menu Users
    Route::group(['prefix' => 'level'], function(){
        Route::get('/', 'LevelController@index');
        Route::post('/', 'LevelController@store');
        Route::get('/create', 'LevelController@create');
        Route::get('/edit/{id}', 'LevelController@edit');
        Route::put('/update/{id}', 'LevelController@update');
        Route::any('/delete/{id}', 'LevelController@delete');
        Route::post('/approved/{id}', 'LevelController@approved');
    });
});
Route::group(['namespace' => 'Master'], function(){
    // Routing Group Menu Users
    Route::group(['prefix' => 'coa'], function(){
        Route::get('/', 'CoaController@index');
        Route::post('/', 'CoaController@store');
        Route::get('/create', 'CoaController@create');
        Route::get('/edit/{id}', 'CoaController@edit');
        Route::get('/detail/{id}', 'CoaController@detail');
        Route::put('/update/{id}', 'CoaController@update');
        Route::any('/delete/{id}', 'CoaController@delete');
        Route::post('/approved/{id}', 'CoaController@approved');
    });
});